package com.leadchampion.integrations;

import com.vtiger.api.VtigerConnect;
import com.vtiger.api.VtigerConstants;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import static com.leadchampion.integrations.IntegrationsCrmFilter.CrmFilterOperator.EQUAL;
import static com.vtiger.api.VtigerConstants.*;


/***************************************************
 *  DOCUMENTAZIONE:
 *  https://www.vtiger.com/docs/rest-api-for-vtiger
 ***************************************************/

public class VtigerIntegrationsCrmDAO implements IntegrationsCrmDAO {

	private final static Logger logger = LogManager.getLogger(VtigerIntegrationsCrmDAO.class);

	protected static VtigerConnect connect;


	public VtigerIntegrationsCrmDAO(String _endpoint, String _username, String _password)
		throws Exception
	{
		connect = new VtigerConnect(_endpoint, _username, _password);
	}

	@Override
	public IntegrationsCrmResponse getEntitiesList()
		throws Exception
	{
		IntegrationsCrmResponse response = connect.vtigerApiCall(VTIGER_OPERATION_LISTTYPES, null, null);

		if (response.isSuccess()) {
			JSONArray entities = new JSONArray();
			JSONArray jsTypes = ((JSONObject) response.getResponseBody()).getJSONArray("types");
			for (int i = 0; i < jsTypes.length();i++) {
				JSONObject entityJSON = new JSONObject();
				entityJSON.put(IntegrationsCrmDAO.CRM_ENTITY_NAME_FIELDNAME, jsTypes.getString(i));
				entities.put(entityJSON);
			}
			response.setResponseBody(entities);
		}
		return response;
	}

	@Override
	public IntegrationsCrmResponse countEntityItems(String entityName)
		throws Exception {
		String queryParams = "query=" + URLEncoder.encode("SELECT count(*) FROM " + entityName + ";", "UTF-8");
		IntegrationsCrmResponse response = connect.vtigerApiCall(VTIGER_OPERATION_QUERY, queryParams, null);

		if (response.isSuccess()) {
			Integer count = Integer.parseInt(((JSONArray) response.getResponseBody())
										.getJSONObject(0).getString("count")
									);
			response.setResponseBody(count);
		}
		return response;
	}

	@Override
	public IntegrationsCrmResponse getEntityItems(String entityName, Integer offset, Integer count)
		throws Exception
	{
		return getEntityItems(entityName, null, offset, count);
	}

	@Override
	public IntegrationsCrmResponse getEntityItems(String entityName, IntegrationsCrmFilters filters, Integer offset, Integer count)
		throws Exception
	{
		/***********************************************************
		 *  DOCUMENTAZIONE:
		 *  https://www.vtiger.com/docs/rest-api-for-vtiger#/Query
		 ***********************************************************/


		String query = "SELECT * FROM " + entityName;

		if (filters != null
		&&  filters.getFilters() != null
		&&  filters.getFilters().size() > 0) {
			query += " WHERE ";
			String filterValue = "";
			for (IntegrationsCrmFilter filter : filters.getFilters()) {
				if (StringUtils.isNotEmpty(filterValue)) {
					filterValue += filters.getBooleanOperator() == IntegrationsCrmFilters.CrmFiltersOperator.AND ? " and " : " or ";
				}
				String operator;
				switch (filter.getOperator()) {
					case CONTAINS:				operator = VtigerConstants.VTIGER_QUERY_OPERATOR_CONTAINS; break;
					case EQUAL:					operator = VtigerConstants.VTIGER_QUERY_OPERATOR_EQUAL; break;
					case NOT_EQUAL:				operator = VtigerConstants.VTIGER_QUERY_OPERATOR_NOT_EQUAL; break;
					case GREATER_THAN:			operator = VtigerConstants.VTIGER_QUERY_OPERATOR_GREATER_THAN; break;
					case GREATER_THAN_OR_EQUAL:	operator = VtigerConstants.VTIGER_QUERY_OPERATOR_GREATER_THAN_OR_EQUAL; break;
					case LOWER_THAN:			operator = VtigerConstants.VTIGER_QUERY_OPERATOR_LOWER_THAN; break;
					case LOWER_THAN_OR_EQUAL:	operator = VtigerConstants.VTIGER_QUERY_OPERATOR_LOWER_THAN_OR_EQUAL; break;
					default: throw new RuntimeException("operator "+filter.getOperator()+" not supported");
				}
				Object fieldValue = filter.getFieldValue();
				switch (fieldValue.getClass().getSimpleName()) {
					case "String":
						fieldValue = "'"+fieldValue+"'";
						break;
					default:
						fieldValue = fieldValue.toString();
				}
				filterValue += " "+filter.getFieldName()+" "+operator+" "+fieldValue;
			}
			query += filterValue;
		}

		if (offset != null
		||  count  != null) {
			if (offset == null) {
				offset = 0;
			}
			if (count == null) {
				count = DEFAULT_MAX_NUM__QUERY_RESULTS;
			}
			query += " limit "+offset + "," + count;
		}
		query +=";";

		String queryParams = "query="+URLEncoder.encode(query,"UTF-8");


		IntegrationsCrmResponse response = connect.vtigerApiCall(VTIGER_OPERATION_QUERY, queryParams, null);
		return response;
	}

	@Override
	public IntegrationsCrmResponse getEntityItem(String entityName, String entityId)
		throws Exception
	{
		IntegrationsCrmFilters filters = new IntegrationsCrmFilters();
		filters.addFilter("id", IntegrationsCrmFilter.CrmFilterOperator.EQUAL,entityId);
		IntegrationsCrmResponse response = getEntityItems(entityName, filters, null,null);
		if (response.isSuccess()) {
			response.setResponseBody( ((JSONArray) response.getResponseBody()).get(0) );
		}
		return response;
	}

	@Override
	public IntegrationsCrmResponse deleteEntityItem(String entityName, String entityId, boolean returnDeletedItem)
		throws Exception
	{
		IntegrationsCrmResponse preDeleteResponse = null;
		if (returnDeletedItem) {
			preDeleteResponse = getEntityItem(entityName,entityId);
		}

		Map<String, String> params = new HashMap();
		params.put("id", entityId);
		IntegrationsCrmResponse response = connect.vtigerApiCall(VTIGER_OPERATION_DELETE, entityName, params);
		logger.debug("deleteEntityItem - CRM delete response:" + response.toString());

		if (response.isSuccess() && returnDeletedItem) {
			logger.info("deleteEntityItem - CRM delete response:" + response.toString());
			return preDeleteResponse;
		}
		return response;
	}

	@Override
	public IntegrationsCrmResponse createEntityItem(String entityName, JSONObject entityJson, boolean returnCreatedItem)
		throws Exception
	{
		entityJson.put("assigned_user_id", connect.getUid());
		Map<String, String> bodyParams = new HashMap();
		bodyParams.put("elementType", entityName);
		bodyParams.put("element", entityJson.toString());
		IntegrationsCrmResponse response = connect.vtigerApiCall(VTIGER_OPERATION_CREATE, null, bodyParams);
		logger.debug("createEntityItem - CRM create response:" + response.toString());

		// indipendentemente dal valore di returnCreatedItem, l'api di creazione restituisce sempre l'elemento creato
		return response;
	}

	@Override
	public IntegrationsCrmResponse updateEntityItem(String entityName, String entityId, JSONObject entityJson, boolean returnUpdatedItem)
		throws Exception
	{
		entityJson.put("assigned_user_id", connect.getUid());
		entityJson.put("id",entityId);

		Map<String, String> bodyParams = new HashMap();
		bodyParams.put("elementType", entityName);
		bodyParams.put("element", entityJson.toString());

		IntegrationsCrmResponse response = connect.vtigerApiCall(VTIGER_OPERATION_UPDATE, null, bodyParams);
		logger.debug("updateEntityItem - CRM update response:" + response.toString());

		if (response.isSuccess() && returnUpdatedItem) {
			return getEntityItem(entityName,entityId);
		}
		return response;
	}

	@Override
	public IntegrationsCrmResponse createVisitAsCompanyNote(String companyEntityName, String crmCompanyId, String noteBody) throws Exception {
		throw new UnsupportedOperationException("Funzione non supportata.");
	}
}