package com.vtiger.api.Models;

public class VtigerSessionResponse {
    private Boolean success;
    private VtigerApiError error;
    private VtigerSessionResult result;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public VtigerApiError getError() {
        return error;
    }

    public void setError(VtigerApiError error) {
        this.error = error;
    }

    public VtigerSessionResult getResult() {
        return result;
    }

    public void setResult(VtigerSessionResult result) {
        this.result = result;
    }
}
