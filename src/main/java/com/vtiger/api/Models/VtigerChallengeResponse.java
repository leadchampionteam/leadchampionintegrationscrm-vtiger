package com.vtiger.api.Models;

public class VtigerChallengeResponse {
	private Boolean success;
	private VtigerApiError error;
	private VtigerChallengeResult result;

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public VtigerApiError getError() {
		return error;
	}

	public void setError(VtigerApiError error) {
		this.error = error;
	}

	public VtigerChallengeResult getResult() {
		return result;
	}

	public void setResult(VtigerChallengeResult result) {
		this.result = result;
	}
}
