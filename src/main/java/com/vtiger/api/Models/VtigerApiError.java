package com.vtiger.api.Models;

public class VtigerApiError {
	private String code;
	private String message;

	public VtigerApiError() {
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
