package com.vtiger.api.Models;

public class VtigerResponse {
	private Boolean success;
	private Object result;
	private VtigerApiError error;

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public VtigerApiError getError() {
		return error;
	}

	public void setError(VtigerApiError error) {
		this.error = error;
	}
}
