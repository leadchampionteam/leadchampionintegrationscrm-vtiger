package com.vtiger.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.leadchampion.integrations.IntegrationsCrmResponse;
import com.vtiger.api.Models.VtigerChallengeResponse;
import com.vtiger.api.Models.VtigerSessionResponse;
import com.vtiger.api.Models.VtigerResponse;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.DatatypeConverter;
import okhttp3.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;

public class VtigerConnect {
	private final static Logger logger = LogManager.getLogger(VtigerConnect.class);

	protected VtigerSessionResponse session;
	protected OkHttpClient httpClient;
	protected String endpoint;
	protected String username;
	protected String password;


	public VtigerConnect(String _endpoint, String _username, String _password) throws Exception {
		this.endpoint = _endpoint;
		this.username = _username;
		this.password = _password;
		this.httpClient = new OkHttpClient();
		this.initSession();
	}

	public String getUid() {
		return this.session.getResult().getUserId();
	}

	protected void initSession() throws Exception {

		String url = this.endpoint + "?operation=getchallenge&username=" + this.username;
		VtigerSessionResponse sessionResponse = null;

		Request request = new Request.Builder().url(url).build();

		logger.debug("initSession start: "+request.toString());

		try {
			Response response = httpClient.newCall(request).execute();

			if (!response.isSuccessful()) {
				String errMsg = "getchallenge error ["+response.code()+"] : "+response.body().toString();
				logger.error(errMsg);
				throw new RuntimeException(errMsg);
			}

			ObjectMapper mapper = new ObjectMapper().configure(FAIL_ON_UNKNOWN_PROPERTIES, false);

			JSONObject jsBody = new JSONObject(response.body().string());
			VtigerChallengeResponse challenge = mapper.readValue(jsBody.toString(), VtigerChallengeResponse.class);

			if (!challenge.getSuccess()) {
				logger.error("initSession: getchallenge error:"+challenge.getError().getMessage());
				return;
			}

			String pwd = challenge.getResult().getToken() + this.password;

			MessageDigest md = MessageDigest.getInstance("MD5");
			String md5Hash = DatatypeConverter.printHexBinary(md.digest(pwd.getBytes(StandardCharsets.UTF_8)));

			url = this.endpoint + "?operation=login";
			RequestBody body = new MultipartBody.Builder()
					.setType(MultipartBody.FORM)
					.addFormDataPart("username", this.username)
					.addFormDataPart("accessKey", md5Hash.toLowerCase())
					.build();

			logger.debug("initSession challenge: "+request.toString());

			request = new Request.Builder().url(url).post(body).build();
			response = httpClient.newCall(request).execute();

			if (!response.isSuccessful()) {
				String errMsg = "getchallenge error ["+response.code()+"] : "+response.body().toString();
				logger.error(errMsg);
				throw new RuntimeException(errMsg);
			}

			jsBody = new JSONObject(response.body().string());
			sessionResponse = mapper.readValue(jsBody.toString(), VtigerSessionResponse.class);

			if (!sessionResponse.getSuccess()) {
				return;
			}

			this.session = sessionResponse;

			logger.debug("initSession response: "+sessionResponse);

		} catch (Exception exception) {
			exception.printStackTrace();
			throw exception;
		}
	}


	public IntegrationsCrmResponse vtigerApiCall(String operation, String queryParms, Map<String, String> bodyParams)
		throws Exception
	{
		if (!this.session.getSuccess()) {
			this.initSession();
		}

		String url = String.format("%s?operation=%s&sessionName=%s",
				this.endpoint,
				operation,
				this.session.getResult().getSessionName()
		);
		if (StringUtils.isNotEmpty(queryParms)) {
			url += "&" + queryParms;
		}

		Request.Builder requestBuilder = new Request.Builder().url(url);
		if (bodyParams != null
		&& bodyParams.keySet().size() > 0) {
			MultipartBody.Builder bodyBuilder = new MultipartBody.Builder().setType(MultipartBody.FORM);
			for (String paramName : bodyParams.keySet()) {
				bodyBuilder.addFormDataPart(paramName, bodyParams.get(paramName));
			}
			logger.debug("preparing POST body: "+bodyParams.toString());
			requestBuilder.post(bodyBuilder.build());
		}

		Request request = requestBuilder.build();

		logger.debug("vtigerApiCall request: "+request.toString());

		try {
			Response httpResponse = httpClient.newCall(request).execute();
			IntegrationsCrmResponse crmResponse = new IntegrationsCrmResponse(httpResponse);	// httpResponse.body() è uno stream e il contenuto viene caricato in crmResponse.responseBody

			ObjectMapper mapper = new ObjectMapper().configure(FAIL_ON_UNKNOWN_PROPERTIES, false);

			if (crmResponse.isSuccess()) {
				// se http ok, parsifichiamo vtigerResponse
				JSONObject jsBody = (JSONObject) crmResponse.getResponseBody();
				VtigerResponse vtigerResponse = mapper.readValue(jsBody.toString(), VtigerResponse.class);

				if (!vtigerResponse.getSuccess()
				&&   vtigerResponse.getError().getCode().equals("INVALID_SESSIONID")) {
					// se la sessione è scaduta provo a riaprirla
					this.initSession();
					httpResponse = httpClient.newCall(request).execute();
					crmResponse = new IntegrationsCrmResponse(httpResponse);
					if (crmResponse.isSuccess()) {
						// se http ok, parsifichiamo vtigerResponse
						jsBody = (JSONObject) crmResponse.getResponseBody();
						vtigerResponse = mapper.readValue(jsBody.toString(), VtigerResponse.class);
					}
				}

				if (vtigerResponse.getSuccess()) {
					// in crmResponse.responseBody carico il valore del campo vtiger result
					// ma non usiamo direttamente vtigerResponse.getResult() perché è già stato trasformato dal mapper e da problemi
					JSONObject jsVtigerResponse = (JSONObject) crmResponse.getResponseBody();
					crmResponse.setResponseBody(jsVtigerResponse.get("result"));
				} else {
					crmResponse.setSuccess(false);
					crmResponse.setResponseBody(vtigerResponse.getError().getCode()+
												" - "+
												vtigerResponse.getError().getMessage()
											);
				}
			}

			return crmResponse;
		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return null;
	}

	protected void finalize() throws Exception {
		Map<String, String> postParams = new HashMap();
		postParams.put("sessionName", this.session.getResult().getSessionName());
		vtigerApiCall("logout",null,postParams);
	}
}