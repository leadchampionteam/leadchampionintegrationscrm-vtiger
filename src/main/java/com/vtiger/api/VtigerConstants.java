package com.vtiger.api;

public class VtigerConstants {
	public final static int DEFAULT_MAX_NUM__QUERY_RESULTS = 1000;

	public final static String VTIGER_OPERATION_CREATE = "create";
	public final static String VTIGER_OPERATION_DELETE = "delete";
	public final static String VTIGER_OPERATION_LISTTYPES = "listtypes";
	public final static String VTIGER_OPERATION_QUERY = "query";
	public final static String VTIGER_OPERATION_UPDATE = "update";

	public final static String VTIGER_QUERY_OPERATOR_EQUAL = "=";
	public final static String VTIGER_QUERY_OPERATOR_GREATER_THAN = ">";
	public final static String VTIGER_QUERY_OPERATOR_LOWER_THAN = "<";
	public final static String VTIGER_QUERY_OPERATOR_CONTAINS = "in";
	public final static String VTIGER_QUERY_OPERATOR_GREATER_THAN_OR_EQUAL = ">=";
	public final static String VTIGER_QUERY_OPERATOR_LOWER_THAN_OR_EQUAL = "<=";
	public final static String VTIGER_QUERY_OPERATOR_NOT_EQUAL = "!=";
	public final static String VTIGER_QUERY_OPERATOR_LIKE = "like";
}