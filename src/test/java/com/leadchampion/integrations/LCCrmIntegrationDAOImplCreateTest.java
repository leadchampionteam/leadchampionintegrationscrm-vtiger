package com.leadchampion.integrations;

import java.util.Scanner;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import static com.leadchampion.integrations.IntegrationsCrmFilter.CrmFilterOperator.EQUAL;
import static com.leadchampion.integrations.IntegrationsCrmFilter.CrmFilterOperator.GREATER_THAN;
import static com.leadchampion.integrations.IntegrationsCrmFilters.CrmFiltersOperator.AND;


public class LCCrmIntegrationDAOImplCreateTest {
	static String entrypoint = "https://crm-test.myti.it/webservice.php";
	static String username = "rete";
	static String password = "5JgN8AU7rI1FyzEz";

	VtigerIntegrationsCrmDAO lcc;

	@Before
	public void init() throws Exception {
		lcc = new VtigerIntegrationsCrmDAO(entrypoint,username,password);
	}

	@Test
	public void test() throws Exception {
		IntegrationsCrmResponse response = lcc.createEntityItem(
											"Leads",
														new JSONObject("{\n" +
																"\t\"leadid\":\"159046\",\n" +
																"\t\"company\":\"Universita degli Studi di Torino\",\n" +
																"\t\"lane\":\"10124, Via Verdi 8\",\n" +
																"\t\"city\":\"Torino\",\n" +
																"\t\"state\":\"Piemonte\",\n" +
																"\t\"noofemployees\": \"349\",\n" +
																"\t\"lastname\":\"redazioneweb@unito.it\",\n" +
																"\t\"phone\":\"+39-011-6706111\",\n" +
																"\t\"fax\":\"+39-011-6706110\",\n" +
																"\t\"website\":\"https:\\/\\/www.unito.it\",\n" +
																"\t\"annualrevenue\": \"40451000\",\n" +
																"\t\"tags\":\"HomeAdChange;Landing Page discover;Pricing;PROVA\"\n" +
															"}"),
											false
													);

		System.out.println(response.getResponseBody());
	}

	public static void main(String[] args)  throws Exception {
		VtigerIntegrationsCrmDAO lcc = new VtigerIntegrationsCrmDAO(entrypoint,username,password);

		Scanner reader = new Scanner(System.in);
		JSONObject entityItem = new JSONObject();

		System.out.println("\n\n***CREAZIONE DI UNA LEAD ***\n\n");
		for (String fieldName : (new String[] {"lastname","firstname","company","city"})) {
			System.out.print(fieldName+" : ");
			entityItem.put(fieldName, reader.nextLine());
		}
		IntegrationsCrmResponse response = lcc.createEntityItem("Leads",entityItem,false);

		System.out.println(response.getResponseBody());
	}

	@Test
	public void testEntitiesList() throws Exception {
		JSONArray jsArray = (JSONArray) lcc.getEntitiesList().getResponseBody();

		System.out.println(jsArray.toString(4));
	}

	@Test
	public void testCount() throws Exception {
		JSONArray jsArray = (JSONArray) lcc.getEntitiesList().getResponseBody();

		for (int i = 0; i < jsArray.length(); i++) {
			String entityName = jsArray.getJSONObject(i).getString(IntegrationsCrmDAO.CRM_ENTITY_NAME_FIELDNAME);

			IntegrationsCrmResponse response = lcc.countEntityItems(entityName);
			if (response.isSuccess()) {
				Integer count = Integer.parseInt(response.getResponseBody().toString());
				System.out.println("# of '" + entityName + "' = " + count);
			} else {
				System.err.println("countEntityItems error: "+response.getResponseBody());
			}
		}
	}

	@Test
	public void testGetAllItems() throws Exception {
		JSONArray jsArray = (JSONArray) lcc.getEntitiesList().getResponseBody();

		for (int i = 0; i < jsArray.length(); i++) {
			String entityName = jsArray.getJSONObject(i).getString(IntegrationsCrmDAO.CRM_ENTITY_NAME_FIELDNAME);


			IntegrationsCrmResponse response = lcc.getEntityItems(entityName,null,null);
			if (response.isSuccess()) {
				JSONArray entityItems = (JSONArray) lcc.getEntitiesList().getResponseBody();
				System.out.println("--------------------------------\n\tItems of '" + entityName + "':\n" + entityItems.toString(4));
			} else {
				System.err.println("testGetAllItems error: "+response.getResponseBody().toString());
			}
		}
	}

	@Test
	public void testGetFilteredItemsEquals() throws Exception {
		String entityName = "Leads";
		IntegrationsCrmFilters filters = new IntegrationsCrmFilters(AND);
		filters.addFilter("id", EQUAL, "10x30");
		JSONArray entityItems = (JSONArray) lcc.getEntityItems(entityName,filters,null,null).getResponseBody();
		System.out.println("--------------------------------\n\tItems of '" + entityName + "':\n" + entityItems.toString(4));
	}

	@Test
	public void testGetFilteredItems() throws Exception {
		String entityName = "Leads";
		IntegrationsCrmFilters filters = new IntegrationsCrmFilters();
		filters.addFilter("id",GREATER_THAN,"10x0");
		JSONArray entityItems = (JSONArray) lcc.getEntityItems(entityName,filters,null,null).getResponseBody();
		System.out.println("--------------------------------" +
							"\n\tItems of '" + entityName + "':\n" + entityItems.toString(4));
	}

	@Test
	public void testGetFilteredItems2() throws Exception {
//		JSONArray jsArray = new JSONArray(lcc.getEntitiesList().getResponseBody().toString());

		String entityName = "Leads";
		IntegrationsCrmFilters filters = new IntegrationsCrmFilters();
		filters.addFilter("company",GREATER_THAN,"A");
		JSONArray entityItems = (JSONArray) lcc.getEntityItems(entityName,filters,null,null).getResponseBody();
		System.out.println("--------------------------------" +
							"\n\tItems of '" + entityName + "':\n" + entityItems.toString(4));
	}
}