package com.leadchampion.integrations;

import org.codehaus.jettison.json.JSONObject;
import org.junit.Test;


public class LCCrmIntegrationDAOImplUpdateTest {
	@Test
	public void test() throws Exception {
		VtigerIntegrationsCrmDAO lcc = new VtigerIntegrationsCrmDAO("https://crm-test.myti.it/webservice.php",
				"rete", "5JgN8AU7rI1FyzEz");
		IntegrationsCrmResponse response = lcc.updateEntityItem("Leads", "10x24",
				new JSONObject("{\n" +
						"   \"leadid\":\"159046\",\n" +
						"   \"company\":\"Universita degli Studi di Torino 123 --- TEST\",\n" +
						"   \"lane\":\"10124, Via Verdi 8\",\n" +
						"   \"city\":\"Torino\",\n" +
						"   \"state\":\"Piemonte\",\n" +
						"   \"noofemployees\": \"349\",\n" +
						"   \"lastname\":\"redazioneweb@unito.it\",\n" +
						"   \"phone\":\"+39-011-6706111\",\n" +
						"   \"fax\":\"+39-011-6706110\",\n" +
						"   \"website\":\"https:\\/\\/www.unito.it\",\n" +
						"   \"annualrevenue\": \"40451000\",\n" +
						"   \"tags\":\"HomeAdChange;Landing Page discover;Pricing;PROVA\"\n" +
						"}"),
				false);

		System.out.println(response.getResponseBody());
	}
}
