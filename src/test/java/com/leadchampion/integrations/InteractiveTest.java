package com.leadchampion.integrations;

import com.leadchampion.integrations.tester.IntegrationsCrmTester;


public class InteractiveTest {

	public static void main(String[] args) throws Exception {

		IntegrationsCrmDAO crmDAO = new VtigerIntegrationsCrmDAO(
											"https://crm-test.myti.it/webservice.php",
											"rete",
											"5JgN8AU7rI1FyzEz"
										);
		System.out.println("# of leads = "	  +crmDAO.countEntityItems("Leads"));

		/** Launch Interactive Tester **/
		new IntegrationsCrmTester(crmDAO);
	}
}
