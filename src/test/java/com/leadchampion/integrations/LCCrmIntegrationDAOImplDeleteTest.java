package com.leadchampion.integrations;

import org.junit.Test;


public class LCCrmIntegrationDAOImplDeleteTest {
	@Test
	public void test() throws Exception {
		VtigerIntegrationsCrmDAO lcc = new VtigerIntegrationsCrmDAO(
											"https://crm-test.myti.it/webservice.php",
											"rete",
											"5JgN8AU7rI1FyzEz"
										);
		IntegrationsCrmResponse response = lcc.deleteEntityItem("Leads", "10x24", false);

		System.out.println(response.getResponseBody());
	}
}
