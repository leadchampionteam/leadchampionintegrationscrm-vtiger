# Lead Champion: Integration with Vtiger open source CRM

## Scopo
Questa libreria ha lo scopo di integrare Lead Champion col CMR open source VTiger.
Sono state integrate le operazioni di Create, Update e Delete. Vtiger non permette di fare retrieve filtrato oppure un retrieve di massa.

## Technical
La versione di VTiger usata è la 7.3, l'ultima rilasciata.
La documentazione di riferimento è https://www.vtiger.com/docs/rest-api-for-vtiger

Non sono gestiti i tags, in quanto da creare manualmente.
Di default le lead vengono create ed assegnate al utente con cui si creano le lead.


 * Ciclo chiamate API

    ```[GET] Challenge -> [POST] Login -> [GET/POST] Operation```
    
    La risposta è sempre un JSON strutturato come segue:
    
    * se è andato a buon fine
        ```
        {
            "success": true,
            "result": {
                id: "", 
                // resto del oggetto creato/aggiornato
            }
        }
        ```
    
    * c'è stato un errore
        ```
        {
            "success": false,
            "error": {
                "code": "",
                "message": ""
            }
        }
        ```
   
Sono state predisposte Classi che modellano gli oggetti risultanti dalle chiamte oppure da usare per le chiamate.


* Gli id sono di tipo String e seguono il seguente pattern ```numxnum``` e sono presesnti in ogni risposta **crete/update**.


* La libreria gestisce la scadenza della sessione


# Example

Sono state predisposte 3 classi di JUnit per le operazioni di Create, Update e Delete,
e il programma di test interattivo disponibile della libreria LeadChampionCRM 
